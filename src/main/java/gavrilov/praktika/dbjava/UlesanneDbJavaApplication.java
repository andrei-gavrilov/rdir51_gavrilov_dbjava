package gavrilov.praktika.dbjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import com.h2database:h2;
//import org.springframework.boot:spring-boot-devtools;

@SpringBootApplication
public class UlesanneDbJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(UlesanneDbJavaApplication.class, args);
	}
}
